#include <stdio.h>

#define MAXLENGTH 16000

int getLine(char s[]);

int getLine(char s[]) {
    int i = 0;
    char c;

    while(( c= getchar()) != EOF && c != '\n' && i < MAXLENGTH) {
        s[i] = c;
        i++;
    }
     s[i] = '\n';
    i++;
    s[i] = '\0';
    return i;
}

int main() {

    printf("Give program sentencies in lines, it will count the chars and reprint line. CTRL-D will start it after sentences.\n");

    int len;
    char line[MAXLENGTH];

    while((len = getLine(line)) > 0) {

         printf("Length: %d ", len);
        for(int i = 0; i < len; i++) {
            putchar(line[i]);
        }
        len = 0;
        printf("\n");
    }
    
}

